# This tool is help you send file in folder to group Telegram.

*Method 1: Run tools with docker.

Create a project folder
```
mkdir sendfile_bot
cd .\sendfile_bot
```

Pull code from repository
```
git clone https://gitlab.com/kinhcantc/bot_telegramsendfile.git
```

Run Docker-compose
```
docker-compose up -d
```

*Method 2: Run tool form code.
*You need to install python3, git before you want to install app.*

Create a project folder
```
mkdir sendfile_bot
cd .\sendfile_bot
```

Pull code from repository
```
git clone https://github.com/kinhcantc/Sendfile_bot.git
```

Install App
```
.\install.ps1
```

Run App
```
.\run.ps1
```

Open browser and access 
```
http://localhost:1000
```

This link helps you want to get token and chat_id [Telegram bot](https://core.telegram.org/bots)



